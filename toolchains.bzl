load("@bazel_tools//tools/build_defs/repo:git.bzl", "git_repository")
load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")

def toolchains_voletaos_dependencies():
    http_archive(
        name = "org_llvm_llvm_x86_64_unknown_linux_gnu",
        build_file = "@org_voletaos_bazel_toolchains_voletaos//:BUILD.llvm",
        sha256 = "1af280e96fec62acf5f3bb525e36baafe09f95f940dc9806e22809a83dfff4f8",
        strip_prefix = "clang+llvm-9.0.1-x86_64-linux-gnu-ubuntu-16.04",
        urls = ["https://github.com/llvm/llvm-project/releases/download/llvmorg-9.0.1/clang+llvm-9.0.1-x86_64-linux-gnu-ubuntu-16.04.tar.xz"],
    )

    http_archive(
        name = "org_llvm_compiler_rt",
        build_file = "@org_voletaos_bazel_toolchains_voletaos//:BUILD.compiler-rt",
        sha256 = "c2bfab95c9986318318363d7f371a85a95e333bc0b34fbfa52edbd3f5e3a9077",
        strip_prefix = "compiler-rt-9.0.1.src",
        urls = ["https://github.com/llvm/llvm-project/releases/download/llvmorg-9.0.1/compiler-rt-9.0.1.src.tar.xz"],
    )

    http_archive(
        name = "org_llvm_libcxx",
        build_file = "@org_voletaos_bazel_toolchains_voletaos//:BUILD.libcxx",
        sha256 = "0981ff11b862f4f179a13576ab0a2f5530f46bd3b6b4a90f568ccc6a62914b34",
        strip_prefix = "libcxx-9.0.1.src",
        urls = ["https://github.com/llvm/llvm-project/releases/download/llvmorg-9.0.1/libcxx-9.0.1.src.tar.xz"],
    )

    http_archive(
        name = "org_llvm_libcxxabi",
        build_file = "@org_voletaos_bazel_toolchains_voletaos//:BUILD.libcxxabi",
        sha256 = "e8f978aa4cfae2d7a0b4d89275637078557cca74b35c31b7283d4786948a8aac",
        strip_prefix = "libcxxabi-9.0.1.src.tar.xz",
        urls = ["https://github.com/llvm/llvm-project/releases/download/llvmorg-9.0.1/libcxxabi-9.0.1.src.tar.xz"],
    )

    http_archive(
        name = "org_llvm_libunwind",
        build_file = "@org_voletaos_bazel_toolchains_voletaos//:BUILD.libunwind",
        sha256 = "535a106a700889274cc7b2f610b2dcb8fc4b0ea597c3208602d7d037141460f1",
        strip_prefix = "libunwind-9.0.1.src",
        urls = ["https://github.com/llvm/llvm-project/releases/download/llvmorg-9.0.1/libunwind-9.0.1.src.tar.xz"],
    )